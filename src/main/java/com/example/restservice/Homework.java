package com.example.restservice;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Homework {

    private final String content;
    private String pattern;
    private DateFormat df;
    private Date today;

    public Homework(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }

    public String getDate() {
        pattern = "MM/dd/yyyy HH:mm:ss";
        df = new SimpleDateFormat(pattern);
        today = Calendar.getInstance().getTime();
        return df.format(today);
    }
}